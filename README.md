# ics-ans-role-elkarbackup

Ansible role to install elkarbackup.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-elkarbackup
```

## License

BSD 2-clause
