import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker(host):
    with host.sudo():
        cmd = host.command("docker ps")
    assert cmd.rc == 0


def test_elkarbackup_containers(host):
    with host.sudo():
        cmd = host.command("docker ps --format '{{.Names}}'")
    assert "elkarbackup" in cmd.stdout
    assert "db" in cmd.stdout
    assert "traefik_proxy" in cmd.stdout


def test_elkarbackup_index(host):
    # This tests that traefik forwards traffic to the elkarbackup web server
    # and that we can access the login page
    cmd = host.command(
        "curl -H Host:$(hostname) -k -L -s https://localhost | grep title"
    )
    assert "        <title>ElkarBackup</title>" in cmd.stdout
